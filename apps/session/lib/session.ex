defmodule Session do
  @moduledoc """
  Documentation for Session.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Session.hello()
      :world

  """
  def hello do
    :world
  end
end
