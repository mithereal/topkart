defmodule Session.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    # List all child processes to be supervised
    children = [
      # Starts a worker by calling: Session.Worker.start_link(arg)
      # {Session.Worker, arg},
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Session.Supervisor]
    Supervisor.start_link(children, opts)
  end

  def version() do
    case :application.get_key(:session, :vsn) do
      {:ok, version} -> to_string(version)
      _ -> "unknown"
    end
  end
end
