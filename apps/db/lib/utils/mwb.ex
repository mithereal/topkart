defmodule DB.Utils.MWB do
  @moduledoc """
  Utils for converting from mysql workbench .mwb to ecto/yacto migration/schemas
  """

  @doc """
  Generate a new migration
  """
  def new(filename) do
    {status,result} = File.read(filename)
    case status do
    :ok ->  new_map = create_map(result)
            map_to_migration(new_map)
    :error -> result
    end
  end

  defp map_to_migration(x) do
  x
  end

  defp create_map(x) do
    XmlToMap.naive_map(x)
  end

end