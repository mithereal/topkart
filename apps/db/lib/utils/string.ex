defmodule DB.Utils.String do
  @moduledoc """
  String utils for the db app
  """

  @doc """
  Convert a string like "     zzz     xxx ccc  " into "zzz xxx ccc"

  ## Examples

      iex> DB.Utils.String.normalize_whitespace "     zzz     xxx ccc  "
      "zzz xxx ccc"
      iex> DB.Utils.String.normalize_whitespace ""
      ""
  """
  def normalize_whitespace(str),
      do: String.replace(String.trim(str), ~r/\s+/, " ")
end
