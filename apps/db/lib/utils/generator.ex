defmodule DB.Utils.Generator do
  @moduledoc """
  Generator utils for the db app
  """

  @doc """
  Generate a new token using :crypto.strong_rand_bytes/1
  """
  def token(length) do
    length
    |> :crypto.strong_rand_bytes()
    |> Base.url_encode64()
    |> binary_part(0, length)
  end
end
