defmodule DB.Schema.Account do
  @moduledoc """
  Represent an account.
  """
  use Ecto.Schema
  import Ecto.Changeset

  schema "accounts" do
    field(:uuid, :string)

    timestamps()
  end

end