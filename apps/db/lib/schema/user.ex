defmodule DB.Schema.User do
  @moduledoc """
  Represent a user that can login on the website.
  """

  use Ecto.Schema
  use Arc.Ecto.Schema
  import Ecto.Changeset

  schema "users" do
    field(:username, :string)
    field(:account_id, :string)
    field(:email, :string)
    field(:encrypted_password, :string)

    # Virtual
    field(:password, :string, virtual: true)

    timestamps()
  end


  @email_regex ~r/\A([\w+\-].?)+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z/i
  @valid_locales ~w(en fr)
  @required_fields ~w(email username)a
  @optional_fields ~w(name password )a

  def email_regex(), do: @email_regex

  @doc """
  Creates a changeset based on the `model` and `params`.
  If no params are provided, an invalid changeset is returned
  with no validation performed.
  """

  def changeset(model, params \\ %{}) do
    model
    |> common_changeset(params)
    |> validate_length(:password, min: 6, max: 256)
    |> put_encrypted_pw
  end


  def changeset_confirm_email(schema, is_confirmed) do
    schema
    |> change(email_confirmed: is_confirmed)
    |> generate_email_verification_token(is_confirmed)
  end

  def password_changeset(schema, params) do
    schema
    |> cast(params, [:password])
    |> validate_required([:password])
    |> validate_length(:password, min: 6, max: 256)
    |> put_encrypted_pw
  end


  def registration_changeset(schema, params \\ %{}) do
    schema
    |> common_changeset(params)
    |> password_changeset(params)
    |> generate_email_verification_token(false)
  end


  @token_length 32
  defp generate_email_verification_token(changeset, false),
       do:
         put_change(
           changeset,
           :email_confirmation_token,
           DB.Utils.Generator.token(@token_length)
         )

  defp generate_email_verification_token(changeset, true),
       do: put_change(changeset, :email_confirmation_token, nil)

  defp common_changeset(schema, params) do
    schema
    |> cast(params, @required_fields ++ @optional_fields)
    |> validate_required(@required_fields)
    |> unique_constraint(:email)
    |> unique_constraint(:username)
    |> update_change(:username, &String.trim/1)
    |> validate_length(:username, min: 5, max: 15)
    |> validate_email()
    |> validate_username()
    |> validate_format(:name, ~r/^[^0-9!*();:@&=+$,\/?#\[\].\'\\]+$/)
  end

  defp put_encrypted_pw(changeset) do
    case changeset do
      %Ecto.Changeset{valid?: true, changes: %{password: pass}} ->
        changeset
        |> put_change(:encrypted_password, Bcrypt.hash_pwd_salt(pass))
        |> delete_change(:password)

      _ ->
        changeset
    end
  end

  def validate_email(changeset = %{changes: %{email: email}}) do
    case Regex.match?(@email_regex, email) do
      true ->
        case Burnex.is_burner?(email) do
          true -> add_error(changeset, :email, "forbidden_provider")
          false -> changeset
        end

      false ->
        add_error(changeset, :email, "invalid_format")
    end
  end

  def validate_email(changeset), do: changeset



  @forbidden_username_keywords ~w(admin newuser temporary deleted support invalid invalid_user invalid_account error account_error)
                                 # Only alphanum, '-' and '_'
  @username_regex ~r/^[a-zA-Z0-9-_]+$/
  defp validate_username(changeset = %{changes: %{username: username}}) do
    lower_username = String.downcase(username)

    case Enum.find(@forbidden_username_keywords, &String.contains?(lower_username, &1)) do
      nil ->
        validate_format(changeset, :username, @username_regex)

      keyword ->
        add_error(changeset, :username, "contains a foridden keyword: #{keyword}")
    end
  end

  defp validate_username(changeset), do: changeset

  # Format name

  defp format_name(nil),
       do: nil

  defp format_name(name) do
    name
    |> String.replace(~r/ +/, " ")
    |> String.trim()
  end
end