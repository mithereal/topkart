defmodule DB.Repo.Migrations.Users do
  use Ecto.Migration

  def change do
    create table(:users) do
      add :username, :citext, null: false
      add :email, :citext, null: false
      add :encrypted_password, :string, null: false
      add :confirmation_token, :string, null: true

      timestamps()
    end

    create unique_index(:users, [:email])
    create unique_index(:users, [:username])
  end
end
