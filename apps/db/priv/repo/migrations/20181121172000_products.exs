defmodule DB.Repo.Migrations.Products do
  use Ecto.Migration

  def change do
    create table(:products) do
      add :name, :citext, null: false
      add :type, :citext, null: false
      add :uuid, :citext, null: false
    end

  end
end
