alias DB.Repo
alias DB.Schema.User
require Logger

# Create Admin in dev or if we're running image locally
if Application.get_env(:db, :env) == :dev do
  Logger.warn("API is running in dev mode. Inserting default user admin")

  admin =
    User.registration_changeset(%User{username: "admin"}, %{
      email: "admin",
      password: "password"
    })

  Repo.insert(admin)
  end