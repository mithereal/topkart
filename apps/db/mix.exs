defmodule Db.MixProject do
  use Mix.Project

  def project do
    [
      app: :db,
      version: "0.1.0",
      build_path: "../../_build",
      config_path: "../../config/config.exs",
      deps_path: "../../deps",
      lockfile: "../../mix.lock",
      elixir: "~> 1.7",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      mod: {DB.Application, []},
      extra_applications: [:logger, :ecto, :postgrex]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      # {:dep_from_hexpm, "~> 0.3.0"},
      # {:dep_from_git, git: "https://github.com/elixir-lang/my_dep.git", tag: "0.1.0"},
      # {:sibling_app_in_umbrella, in_umbrella: true},
      {:arc, github: "Betree/arc", override: true},
      {:arc_ecto, "~> 0.10.0"},
      {:yacto, "~> 1.2"},
      {:ecto, "~> 2.2.8"},
      {:ecto_enum, "~> 1.0"},
      {:postgrex, "~> 0.13.3"},
      {:ex_aws, "~> 2.1"},
      {:ex_aws_s3, "~> 2.0"},
      {:slugger, "~> 0.2"},
      {:bcrypt_elixir, "~> 1.0"},
      {:burnex, "~> 1.0"},
      {:hashids, "~> 2.0"},
      {:xml_builder, "~> 2.0", override: true},
      {:elixir_xml_to_map, "~> 0.1"}
    ]
  end
end
