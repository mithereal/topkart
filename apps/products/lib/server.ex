defmodule Products.Product.Server do
  require Logger

  use GenServer

  @name __MODULE__

  @registry_name :product_registry

  defstruct product: []

  def start_link() do

    state = %__MODULE__{ product: []}

    GenServer.start_link(__MODULE__, [state], name: @name)
  end

  defp via_tuple(data) do

    {:via, Registry, {@registry_name, data}}
  end

  def handle_cast({:show, websocket_pid} ,  state) do
    task = Task.async(fn -> GenServer.cast(websocket_pid, {:add_product, state.product}) end)
    {:noreply, state}
  end

  def handle_call(:list, _from,  state) do
    {:reply, state.product, state}
  end

  def handle_cast({:load, product},   state) do
  #  updated_state = %__MODULE__{ state.product | product}
    {:reply, product, state}
  end

  def init([args]) do
    {:ok, args}
  end

  def load( id, product ) do

    GenServer.cast(via_tuple(id), {:load, product })
  end

end