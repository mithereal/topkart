defmodule Products.Application do
  @moduledoc false

  use Application

  def start(_type, _args) do
    import Supervisor.Spec, warn: false

    # Define workers and child supervisors to be supervised
    children = [
      # Starts a worker by calling: DB.Worker.start_link(arg1, arg2, arg3)
      # worker(DB.Worker, [arg1, arg2, arg3]),
      #supervisor(DB.Repo, [])
    ]

    # See http://elixir-lang.org/docs/stable/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Products.Supervisor]
    Supervisor.start_link(children, opts)
  end

  def version() do
    case :application.get_key(:products, :vsn) do
      {:ok, version} -> to_string(version)
      _ -> "unknown"
    end
  end

  defp load do
    case Application.get_env(:products, :app_mode) do

      "test" -> # query = DB.Product.fetch 10

                    products = []

#                    products = DB.Repo.all Product.Product

                    Enum.each(products, fn(x) ->
                      Product.Supervisor.start(x)
                    end)
                    {:ok, "success"}

      _ ->
        {:ok, "success"}
    end

  end

end